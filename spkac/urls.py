from django.conf.urls import url
from . import views

urlpatterns = [
    url(r"^$", views.StatusView.as_view(), name="spkac_status"),
    url(r"^enroll/$", views.EnrollView.as_view(), name="spkac_enroll"),
    url(r"^enroll_manually/$", views.EnrollManuallyView.as_view(), name="spkac_enroll_manually"),
    url(r"^enroll_csr/$", views.EnrollCSRView.as_view(), name="spkac_enroll_csr"),
    url(r"^revoke/(?P<serial>\d+)$", views.Revoke.as_view(), name="spkac_revoke"),
]
